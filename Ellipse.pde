//A base class to represent a basic ellipse shape which will be the geometric representation of everything.

class Ellipse {
  //Class variables
  Vec2D mPosition; //the 2D position of the centre of the ellipse
  Vec2D mRadii; //the radii of the ellipse
  int mParticleIndex; //the particle index of the particle to which the ellipse is attached. 

  //Constructors
  Ellipse() {
    mPosition = new Vec2D(0.0, 0.0);
    mRadii = new Vec2D(0.0, 0.0);
    mParticleIndex = 0;
  }

  Ellipse(Vec2D iPosition, Vec2D iRadii, int iParticleIndex) {
    mPosition = iPosition.copy();
    mRadii = iRadii.copy();
    mParticleIndex = iParticleIndex;
  }
  
  Ellipse(Ellipse iOther) {
    mPosition = iOther.mPosition.copy();
    mRadii = iOther.mRadii.copy();
    mParticleIndex = iOther.mParticleIndex;
  }

  //Methods
  //Update the ellipse's position with the particle it's attached to
  void updatePosition(VerletPhysics2D iPhysics, float iHeight) {
    VerletParticle2D p = iPhysics.particles.get(mParticleIndex);
    mPosition = p.getPreviousPosition().copy();
    //Make sure the bug doesn't disappear of the picture.
    mPosition.y += mRadii.y*0.9;
    //NOTE: I shouldn't use height below but otherwise the 
    mPosition.y = min(mPosition.y, iHeight - mRadii.y*0.9);
  }

  void setParticleIndex(int iParticleIndex) {
    mParticleIndex = iParticleIndex;
  }

  void setPosition(Vec2D iPosition) {
    mPosition = iPosition.copy();
  }
  
  void setRadii(Vec2D iRadii) {
    mRadii = iRadii;
  }
  
  void setRadii(float iRX, float iRY) {
    mRadii.x = iRX;
    mRadii.y = iRY;
  }

  //For collision detection we will use the following to help find the distance between Ellipses
  float getEllipseSize() {
    return getAvgSize();
    //return max(mRadii.x, mRadii.y);
  }
  
  float getAvgSize(){
    return (mRadii.x + mRadii.y)*0.5;
  }

  //Need to do a plus here because when drawing y is downwards on the processing canvas.
  float getEllipseHighest() {
    return mPosition.y + mRadii.y;
  }

  float getEllipseLowest() {
    return mPosition.y + mRadii.y;
  }

  float getEllipseRight() {
    return mPosition.x + mRadii.x;
  }

  float getEllipseLeft() {
    return mPosition.x - mRadii.x;
  }

  float getWidth() {
    return mRadii.x*2.0;
  }

  float getHalfWidth() {
    return mRadii.x;
  }

  Vec2D getTopLeft() {
    return new Vec2D( mPosition.x - mRadii.x, mPosition.y - mRadii.y);
  }

  void display() {
    fill(175);
    stroke(0);
    ellipse(mPosition.x, mPosition.y, mRadii.x*2.0, mRadii.y*2.0);
  }
}