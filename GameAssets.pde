//A class to store all the game assets so we avoid creating multiple copies of data. 

class GameAssets {
  
  //Variables
  //The array of all the bugs in the game
  ArrayList<Bug> mBugs;
  //The squashedbug object
  SquashedBug mSquashedBug;
  //The lifebug object
  LifeBug mLifeBug;
  //The back button
  BackButton mBackButton;
  
  //Catching Game stuff
  //An array for all the obstacles in the CatchingGame. The first type is the bomb
  ArrayList<Obstacle> mObstacles;
  //The catcher object 
  Catcher mCatcher;
  
  //The type of game, hanging or catching
  GameType mGameType;
  //The background image
  PImage mBackground;
  //The background starting position for this display
  Vec2D mBackgroundPos;
  //The intro screen image. This will become a class with objects later.
  PImage mIntroScreen;
  //The blank version of the intro screen image for high scores and credits.
  PImage mBlankIntroScreen;
  
  //The level the player is on.
  int mGameLevel;
  //Points to next level
  int mNextLevelScore;
        
  //All the sounds
  //A bug sound for appearing  
  MediaPlayer mBugAppear;
  //A bug sound for being scared  . 
  MediaPlayer mLevelUp;
  //The sound for a bug being squashed
  MediaPlayer mSquashedBugSound;
  //The sound for a bomb explosion
  MediaPlayer mExplosion;
  //Intro song
  //MediaPlayer mIntroSong;
  
  //Constructor
  GameAssets(GameType iGameType) {
    mGameType = iGameType;
    mBugs = new ArrayList<Bug>();
    mSquashedBug = new SquashedBug();
    mLifeBug = new LifeBug();
    mGameLevel = 0;
    //The number of points until the next level
    mNextLevelScore = 250;
    mBackgroundPos = new Vec2D();
    mObstacles = new ArrayList<Obstacle>();
    mCatcher = new Catcher();
    mBackButton = new BackButton();
    
  }

  //Methods
  //Remove all the images such as LifeBugs, Bugs, SquashedBugs, Background, Web and Catcher
  void clearGameImages(){
    mObstacles.clear();
    mBugs.clear();
    mSquashedBug.mImages.clear();
    g.removeCache(mBackground);
  }
  
  //Remove all the images such as the Introscreen and BlankIntroscreen.
  void clearIntroImages(){
    g.removeCache(mBlankIntroScreen);
    g.removeCache(mIntroScreen);
  }
  
  void gameReset(){
    mGameLevel = 0;
    mNextLevelScore = 250;
  }
  
  void setGameType(GameType iGameType){
    mGameType = iGameType;
  }
  
  float getAvgSize(int iIndex){
    if (iIndex < mBugs.size()) {
      return mBugs.get(iIndex).getAvgSize();
    }

    assert mGameType == GameType.CATCHING_GAME;
    assert iIndex < mBugs.size() + mObstacles.size();

    int index = iIndex - mBugs.size();
    return mObstacles.get(index).getAvgSize();
    
  }
  
  float getItemWidth(int iIndex) {
    if (iIndex < mBugs.size()) {
      return mBugs.get(iIndex).getBugLook().width;
    }

    assert mGameType == GameType.CATCHING_GAME;
    assert iIndex < mBugs.size() + mObstacles.size();

    int index = iIndex - mBugs.size();
    return mObstacles.get(index).getWidth();
  }
  
  float getItemHeight(int iIndex) {
    if (iIndex < mBugs.size()) {
      return mBugs.get(iIndex).getBugLook().height;
    }

    assert mGameType == GameType.CATCHING_GAME;
    assert iIndex < mBugs.size() + mObstacles.size();

    int index = iIndex - mBugs.size();
    return mObstacles.get(index).getHeight();
  }
  
  void addLifeBugs(CatchingGame iGame){
     //Start the bugs at the right side of the screen.
     float startX = 0.0;
     float startY = 0.0;
     Vec2D position = new Vec2D(startX, startY);
     for(int i=0; i < iGame.mNumberOfLives; ++i){
       Vec2D pos = position.copy();
       LifeBug bug = new LifeBug(pos, mLifeBug.getLook());
       iGame.addLifeBug(bug);
       position.x += mLifeBug.getWidth();
     }
  }
  
  void addLifeBugs(HangingGame iGame){
     //Start the bugs at the right side of the screen.
     float startX = 0.0;
     float startY = 0.0;
     Vec2D position = new Vec2D(startX, startY);
     for(int i=0; i < iGame.mNumberOfLives; ++i){
       Vec2D pos = position.copy();
       LifeBug bug = new LifeBug(pos, mLifeBug.getLook());
       iGame.addLifeBug(bug);
       position.x += mLifeBug.getWidth();
     }
  }

  void addItem(CatchingGame iGame, int iIndex, Ellipse iEllipse) {
    if (iIndex < mBugs.size()) {
      Bug bug = new Bug(mBugs.get(iIndex).mWeightForce, iEllipse, mBugs.get(iIndex).mImages);
      iGame.addBug(bug);
      Util.playSound(mBugAppear);
      return;
    }
    //If index is greater than the number of bugs then we must be adding an obstacle
    int index = iIndex - mBugs.size();
    assert index < 2;
    if(index == 0){
      Bomb localBomb = (Bomb)mObstacles.get(index);
      Bomb bomb = new Bomb(localBomb.mWeightForce, iEllipse, localBomb.mImages);
      iGame.addObstacle(bomb);
    }else{
     //index = 1.
      SpiderWeb localWeb = (SpiderWeb)mObstacles.get(index);
      SpiderWeb web = new SpiderWeb(mObstacles.get(index).mWeightForce, iEllipse, localWeb.mImage);
      iGame.addObstacle(web);
    }
  }

  void addItem(HangingGame iGame, int iIndex, Ellipse iEllipse) {
    assert iIndex < mBugs.size();
    if (iIndex < mBugs.size()) {
      Bug bug = new Bug(mBugs.get(iIndex).getWeightForce(), iEllipse, mBugs.get(iIndex).mImages);
      iGame.addBug(bug);
    }
  }

  PImage getNextImage(int iIndex) {
    if (iIndex < mBugs.size()) {
      return mBugs.get(iIndex).getBugLook();
    }
    
    //If the index is greater then we must be here.
    assert mGameType == GameType.CATCHING_GAME;
    assert iIndex < mBugs.size() + mObstacles.size();
    int index = iIndex - mBugs.size();
    return mObstacles.get(index).getLook();
  }
  
  boolean levelUp(float iGameScore){
    if(iGameScore >= mNextLevelScore){
     
      Util.playSound(mLevelUp);
      mGameLevel++; 
      int prevLevelScore = mNextLevelScore;
      int levelIncrement = 200;
      mNextLevelScore = prevLevelScore + mGameLevel*levelIncrement;
      return true;
    }
    return false;
  }
}