/// A class to displpay all the buttons on the introduction screen and check for when they are pressed.

class IntroButtons{
  //Variables
  ArrayList<Rect> mButtonShapes;
  ArrayList<Boolean> mIsButtonPressed;
  ArrayList<String> mButtonText;
  int mNumberOfButtons;
  //The delay in milliseconds between button presses for any intro buttons
  int mButtonPressDelay;
  //The time the button was last pressed.
  long mLastButtonPress;
  
  //Constructor
  IntroButtons(float iFontSize, float iScreenHeight, float iScreenWidth){
    mNumberOfButtons = 5;
    mButtonPressDelay = 1000;
    mLastButtonPress = 0;
    mIsButtonPressed = new ArrayList<Boolean>(mNumberOfButtons);
    for(int i=0; i<mNumberOfButtons; ++i){
      mIsButtonPressed.add(false);
    }
    
    mButtonText = new ArrayList<String>(mNumberOfButtons);
    mButtonText.add("Hang");
    mButtonText.add("Catch");
    mButtonText.add("Scores");
    mButtonText.add("Credits");
    mButtonText.add("Help");
    
    mButtonShapes = new ArrayList<Rect>();
    float extraSpace = 5.0;
    float extraLetterSpace = 1.4;
    float startYPos = iScreenHeight - extraLetterSpace*iFontSize - extraSpace;
    float displacement = 25.0;
    for(int i=0; i < 4; ++i){
      Rect button;
      //Position all buttons next to each other down the bottom of the screen. Make sure they are big enough for the text.
      button = new Rect(displacement, startYPos, mButtonText.get(i).length()*iFontSize*0.75, extraLetterSpace*iFontSize); 
      mButtonShapes.add(button);
      displacement += mButtonText.get(i).length()*iFontSize*0.85;
    } 
    //Help button is added to the top right of the screen.
    Rect button;
    //Position all buttons next to each other down the bottom of the screen. Make sure they are big enough for the text.
    float buttonWidth = mButtonText.get(4).length()*iFontSize*0.75;
    button = new Rect(iScreenWidth - buttonWidth, extraLetterSpace*iFontSize*0.5, buttonWidth, extraLetterSpace*iFontSize); 
    mButtonShapes.add(button);
  }
  
  //Methods
  void display(){
    textAlign(CENTER);
    stroke(165,82,8);
    for(int i=0; i < mButtonShapes.size(); ++i){
      fill(255); 
      //The x and y coordinates of the rect are the top left. 
      Rect b = mButtonShapes.get(i);
      rect(b.x, b.y, b.width, b.height, 7);
      fill(165,82,8);
      text(mButtonText.get(i), b.x + b.width*0.5, b.y + b.height*0.8);
    }
  }
  
  void reset(){
    for(int i=0; i<mIsButtonPressed.size(); ++i){
      mIsButtonPressed.set(i, false);
    }
  }
  
  void pressed(Vec2D iMouseLoc){
    long currentTime = millis();
    //Ignore quick button presses
    if(mLastButtonPress + mButtonPressDelay > currentTime){
      return;
    }
    
    
    //Test if a button was pressed and set to true/false respectively
    for(int i=0; i<mButtonShapes.size(); ++i){
      if(mButtonShapes.get(i).containsPoint(iMouseLoc)){
        mIsButtonPressed.set(i, true);
        mLastButtonPress = currentTime;
      }
    }
  }
  
  boolean pressHangButton(){
    return mIsButtonPressed.get(0);
  }
  
  boolean pressCatchButton(){
    return mIsButtonPressed.get(1);
  }
  
  boolean pressScoreButton(){
    return mIsButtonPressed.get(2);
  }
  
  boolean pressCreditsButton(){
    return mIsButtonPressed.get(3);
  }
  
  boolean pressHelpButton(){
    return mIsButtonPressed.get(4);
  }
  
  
}