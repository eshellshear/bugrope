//A class for a specific obstacle, a spider web. It only stops the player from moving and does not make them lose a life.

class SpiderWeb extends Obstacle {

  PImage mImage;

  //Constructor
  SpiderWeb() {
    //SpiderWebs all have the same fixed size.
    super();
  }
  
  SpiderWeb(Vec2D iWeight, Ellipse iEllipse, PImage iImage) {
    super(iWeight, iEllipse);
    mImage = iImage;
  }

  //Methods
  //It disappears as soon as it touches the Catcher which then becomes covered in it for X amount of time.
  boolean isFinished() {
    return mCaught;
  }

  boolean isBomb() {
    return false;
  }

  boolean isWeb() {
    return true;
  }

  PImage getImage() {
    return mImage;
  }
  
  float getWidth(){
   return mImage.width; 
  }
  
  float getHeight(){
   return mImage.height; 
  }
  
  float getAvgSize(){
    return mEllipse.getAvgSize();
  }
  
  PImage getLook() {
    return mImage;
  }

  
  void addImage(String iPath) {
    mImage = loadImage(iPath);
  }

  
  void display(float iFrameCount) {
    Vec2D topLeft = super.mEllipse.getTopLeft();
    image(mImage, topLeft.x, topLeft.y);
  }
}