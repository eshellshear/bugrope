//The back button in the game to go back to the Intro screen. 

class BackButton{
  //Variables
  Rect mShape;
  PImage mImage;
  
  //Constructor
  BackButton(){
    mShape = new Rect();
  }
  
  //Methods
  boolean buttonPressed(Vec2D iPos){
    return mShape.containsPoint(iPos);
  }
  
  void setRectDimensions(){
    Vec2D dimensions = new Vec2D(mImage.width, mImage.height);
    Vec2D position = new Vec2D(width - mImage.width, 0.0);
    setRect(position, dimensions);
    
  }
  
  void setRect(Vec2D iPosition, Vec2D iWidth){
    mShape.setDimension(iWidth);
    mShape.setPosition(iPosition);
  }
  
  PImage getImage() {
    return mImage;
  }
  
  float getWidth(){
   return mImage.width; 
  }
  
  float getHeight(){
   return mImage.height; 
  }
  
  PImage getLook() {
    return mImage;
  }
  
  Vec2D topLeft(){
    return mShape.getTopLeft(); 
  }

  Vec2D bottomRight(){
    return mShape.getBottomRight(); 
  }
  
  void addImage(String iPath) {
    mImage = loadImage(iPath);
    setRectDimensions();
  }
  
  
  void display(){
    Vec2D topLeft = topLeft();
    image(mImage, topLeft.x, topLeft.y);
    
  }
  
}