//A class to load all the images from disk 

class ImageLoader {

  //Variables
  Vec2D mGravity;
  float mScreenWidth;
  String HiRes;
  int extraWidth;
  int mSmallScreenWidth;

  //Constructor
  ImageLoader(Vec2D iGravity, float iWidth) {
    mGravity = iGravity;
    mScreenWidth = iWidth;
    extraWidth = 1;
    mSmallScreenWidth = 1500;
    
    if(mScreenWidth < mSmallScreenWidth){
      HiRes = "";
    }else {
      HiRes = "HiRes";
    }
  }

  //Methods
  //Just the images for the intro screens.
  //WARNING: Must have loaded the back button.
  void loadIntroImages(GameAssets iAssets){
    loadBlankIntroScreen(iAssets);
    loadIntroScreen(iAssets);
  }
  
  //Just the images for the game.  
  void loadGameImages(int iNumberOfBugs, GameAssets iAssets, int iBackButtonHeight, GameType iGameType){
    loadLifeBug(iAssets.mLifeBug);
    loadBugs(iNumberOfBugs, iAssets.mBugs);
    loadSquashedBug(iAssets.mSquashedBug);
    loadBackground(iAssets, iBackButtonHeight);
    if(iGameType == GameType.CATCHING_GAME){
      //Must load bomb and web in this order!! Bomb first then web!
      loadBomb(iAssets.mObstacles);
      loadWeb(iAssets.mObstacles);
    }
  }
  
  //Load the back button
  void loadBackButton(BackButton iBackButton){
    String fileName = "";
    if(mScreenWidth < mSmallScreenWidth){
      fileName = "backButton.png";
    } else {
      fileName = "backButtonHiRes.png";
    }
    iBackButton.addImage(fileName);
  }

  //Load the background for the game.
  void loadBackground(GameAssets iAssets, int iBackButtonHeight) {
    String fileName = "";
    if(mScreenWidth < 2000){
      fileName = "background1280.png";
    } else {
      fileName = "background2560.png";
    }
        
    try{
      AssetManager assets = context.getResources().getAssets();
      InputStream buffer = new BufferedInputStream((assets.open(fileName)));
      Bitmap bimg = Bitmap.createScaledBitmap(BitmapFactory.decodeStream(buffer), width + extraWidth, height, true);
      iAssets.mBackground = createImage(width + extraWidth, height - iBackButtonHeight, ARGB);
      bimg.getPixels(iAssets.mBackground.pixels, 0, iAssets.mBackground.width, 0, iBackButtonHeight, iAssets.mBackground.width, iAssets.mBackground.height);
      iAssets.mBackground.updatePixels(); 
      bimg.recycle();
    }catch (IOException e1) {
      e1.printStackTrace();
    }
  }

  //Load the picture for the intro screen
  void loadIntroScreen(GameAssets iAssets) {
    String fileName = "";
    if(mScreenWidth < mSmallScreenWidth){
      fileName = "IntroScreen1280.png";
    } else if(mScreenWidth < 2000){
      fileName = "IntroScreen1920.png";
    } else {
      fileName = "IntroScreen2560.png";
    }
    
    try{
      AssetManager assets = context.getResources().getAssets();
      InputStream buffer = new BufferedInputStream((assets.open(fileName)));
      Bitmap bimg = Bitmap.createScaledBitmap(BitmapFactory.decodeStream(buffer), width + extraWidth, height, true);
      iAssets.mIntroScreen = createImage(width + extraWidth, height, ARGB);
      bimg.getPixels(iAssets.mIntroScreen.pixels, 0, iAssets.mIntroScreen.width, 0, 0, iAssets.mIntroScreen.width, iAssets.mIntroScreen.height);
      iAssets.mIntroScreen.updatePixels(); 
      bimg.recycle();
    }catch (IOException e1) {
      e1.printStackTrace();
    }
        
    //iAssets.mIntroScreen = loadImage(fileName);

  }
  
  //Load the picture for the intro screen
  void loadBlankIntroScreen(GameAssets iAssets) {
    String fileName = "";
    if(mScreenWidth < mSmallScreenWidth){
      fileName = "IntroScreenBlank1280.png";
    } else {
      fileName = "IntroScreenBlank2560.png";
    }
    
    try{
      AssetManager assets = context.getResources().getAssets();
      InputStream buffer = new BufferedInputStream((assets.open(fileName)));
      Bitmap bimg = Bitmap.createScaledBitmap(BitmapFactory.decodeStream(buffer), width + extraWidth, height, true);
      iAssets.mBlankIntroScreen = createImage(width + extraWidth, height, ARGB);
      bimg.getPixels(iAssets.mBlankIntroScreen.pixels, 0, iAssets.mBlankIntroScreen.width, 0, 0, iAssets.mBlankIntroScreen.width, iAssets.mBlankIntroScreen.height);
      iAssets.mBlankIntroScreen.updatePixels(); 
      bimg.recycle();
    }catch (IOException e1) {
      e1.printStackTrace();
    }
    //iAssets.mBlankIntroScreen = loadImage(fileName);

  }
  

  //Load the images for the squashed bug. One set of images for all bugs.
  void loadSquashedBug(SquashedBug iSquashedBug) {
    String fileName = "SquashedBug";
    for (int j=0; j < iSquashedBug.getNumberOfImages(); ++j) {
      String filePath = fileName + j + HiRes + ".png";
      iSquashedBug.addImage(filePath);
    }
  }

  //Load the image for the web
  void loadWeb(ArrayList<Obstacle> iObstacles) {
    SpiderWeb web = new SpiderWeb();
    String fileName = "web" + HiRes + ".png";
    web.addImage(fileName);
    getDimensions(web.getImage(), web);
    iObstacles.add(web);
  }
  
  void loadLifeBug(LifeBug iLifeBug){
    String fileName = "lifeBug" + HiRes + ".png";
    iLifeBug.addImage(fileName);
  }

  //Load the images for the bombs
  void loadBomb(ArrayList<Obstacle> iObstacles) {
    Bomb bomb = new Bomb();
    String fileName = "bomb";
    for (int j=0; j <bomb.getNumberOfImages(); ++j) {
      String filePath = fileName + j + HiRes + ".png";
      bomb.addImage(filePath);
    }
    getDimensions(bomb.getLook(), bomb);
    iObstacles.add(bomb);
  }

  //Load the images for the catcher
  void loadCatcher(Catcher iCatcher) {
    String fileName = "Swatter";
    for (int j=0; j < iCatcher.getNumberOfImages(); ++j) {
      String filePath = fileName + j + HiRes + ".png";
      iCatcher.addImage(filePath);
    }
    
    Weight tmpWeight = new Weight();
    getDimensions(iCatcher.getImage(), tmpWeight); 
    iCatcher.setEllipse(tmpWeight.mEllipse);
  }

  //The general function to get the dimensions of the object.
  void getDimensions(PImage iImage, Weight iWeight) {

    //Figure out the dimensions of the object
    float imageWidth = iImage.width;
    float imageHeight = iImage.height;
    float averageDim = (imageWidth + imageHeight)*0.5;
    Vec2D weightForce = new Vec2D(averageDim*mGravity.x, averageDim*mGravity.y);

    iWeight.setWeightForce(weightForce);

    Vec2D ellipseSize = new Vec2D(imageWidth, imageHeight);
    Vec2D startPosition = new Vec2D(0.0, 0.0);
    //The start position and particle index will get set once the particle is assigned.
    Ellipse tempEllipse = new Ellipse(startPosition, ellipseSize, 0);
    iWeight.setEllipse(tempEllipse);
  }

  //Load all the bug images. When a weight is then chosen it is chosen according to the bugs.
  void loadBugs(int iNumberOfBugs, ArrayList<Bug> iBugs) {
    String fileName = "bug";
    for (int i=0; i < iNumberOfBugs; ++i) {
      //Each bug comprises 3 images. The size of the bug depends on the size of the image so we take the rectangle width as 2*XRadius and height as 2*YRadius
      Bug newBug = new Bug();
      //Add each image directly to the bug, avoiding creating anything in between.

      for (int j=0; j <newBug.getNumberOfImages(); ++j) {
        int index = 3*i+j;
        String filePath = fileName + index + HiRes + ".png";
        newBug.addImage(filePath);
      }
      
      getDimensions(newBug.getBugLook(), newBug);
      iBugs.add(newBug);
    }
  }
}