//A bomb obstacle which the player must avoid in the CatchingGame. Once exploded the bomb is removed from the game.

class Bomb extends Obstacle {
  //Variables 
  ArrayList<PImage> mImages;
  int mCurrentImage;
  final int mNumberOfImages = 5;

  //Constructor
  Bomb() {
    //Bombs all have the same fixed size.
    super(); 
    mCurrentImage = 0;
    mImages = new ArrayList<PImage>();
  }
  
  Bomb(Vec2D iWeight, Ellipse iEllipse, ArrayList<PImage> iImages) {
    super(iWeight, iEllipse);
    mCurrentImage = 0;
    mImages = iImages;
  }

  //Methods
  int getNumberOfImages() {
    return mNumberOfImages;
  }

  boolean isFinished() {
    return mCurrentImage == mImages.size() -1;
  }
  
  boolean isExploded(){
   return mCurrentImage > 0; 
  }

  boolean isBomb() {
    return true;
  }

  boolean isWeb() {
    return false;
  }

  PImage getImage() {
    return mImages.get(0);
  }

  float getWidth(){
   return getLook().width; 
  }
  
  float getHeight(){
   return getLook().height; 
  }
  
  float getAvgSize(){
    return mEllipse.getAvgSize();
  }
  
  PImage getLook() {
    return getLook(1);
  }

  PImage getLook(int iIndex) {
    return mImages.get(iIndex);
  }

  
  void addImage(String iPath) {
    PImage image;
    image = loadImage(iPath);
    mImages.add(image);
   }

  
  void display(float iFrameCount) {
    Vec2D topLeft = super.mEllipse.getTopLeft();
    if (super.mCaught == false) {
      //Display bomb
      image(mImages.get(mCurrentImage), topLeft.x, topLeft.y);
    } else {
      //Explode bomb
      image(mImages.get(mCurrentImage), topLeft.x, topLeft.y);
      if(iFrameCount % 5 == 0){
        ++mCurrentImage;
      }
      //Could let it cycle.
      //mCurrentImage = mCurrentImage % mImages.size();
    }
  }
}