import toxi.physics2d.behaviors.*;
import toxi.physics2d.*;
import toxi.geom.*;

//Important:  I assume always that the first and last particles are locked and represent the supports of the clothesline.
//This class has a wrong name. It is simply the class representing the rope. It's name comes from an initial thought of what the game could be like.
class Clothesline {

  //Class Variables
  float mSegmentLengths; //the rest length of each spring connecting two points
  float mSegmentStrengths; //each spring's strength 
  float mTop; //the top of the clothesline
  float mStart; //the x position of the start of the first particle
  float mEnd; //the x position of the end of the first particle
  float mFirstFixedPosition; //the x position of the first 'supporting pole' (rectangle constraint)
  float mSecondFixedPosition; //the x position of the second 'supporting pole' (rectangle constraint)
  float mAttachRadius; //the radius in which a click is counted as clicking on a particle. Outside of this distance clicks are ignored for attaching weights.
  float mMaxSpringLength; //the maximum length allowed of a constrained spring. This is added to prevent the clothesline from over stretching
  VisibleRectConstraint[] mRect; //the rectangle constraints to represent the poles holding up the clothesline.
  //RectConstraint mGroundConstraint; // the constraint for the ground so that the rope won't go through it.
  PImage mRopeTexture; //the file for the texture of the rope.
  PImage mPoleImage; //the image for the pole holding up the rope.

  //Constructor
  Clothesline() {
    //Edit the segment lengths to get stiffer springs.
    mSegmentLengths = 13;
    mSegmentStrengths = 1.5;
    mTop = 60.0;
    mStart = 0.0;
    mEnd = width;
    mFirstFixedPosition = (mEnd - mStart)*0.33;
    mSecondFixedPosition = (mEnd - mStart)*0.66;
    mAttachRadius = 40.0;
    mRect =new VisibleRectConstraint[2];
    mMaxSpringLength = 1.5;
    mRopeTexture = loadImage("rope_texture-hd.png");
    if(width < 1300){
      mPoleImage = loadImage("pole.png");
    } else {
      mPoleImage = loadImage("poleHiRes.png");
    }
    
  }

  //Class Methods

  //Initialize the class and create the clothesline.
  void init(VerletPhysics2D iPhysics, int iNumParticles, float iGroundHeight) {
    //Adjust height to make it over the ground.
    float poleHeight = mPoleImage.height;
    float poleWidth = mPoleImage.width;
    mTop = iGroundHeight - poleHeight;
    float particleStartHeight = mTop - 5.0;
    
    //Create the clothesline here as a bunch of particles connected by lines
    VerletParticle2D startWire  = new VerletParticle2D(mStart, particleStartHeight);
    startWire.lock(); 
    physics.addParticle(startWire);
    float widthf = (float)width;
    float numParticlesf = (float)iNumParticles;

    for (int i=0; i< iNumParticles; i++) {
      VerletParticle2D p  = new VerletParticle2D((float)i*widthf/numParticlesf, particleStartHeight);
      VerletParticle2D prev = iPhysics.particles.get(physics.particles.size()-1);
      // create a spring between the previous and the current particle of length 10 and strength 1
      VerletConstrainedSpring2D s = new VerletConstrainedSpring2D(p, prev, mSegmentLengths, mSegmentStrengths, mMaxSpringLength);
      // add the spring to the physics system
      iPhysics.addSpring(s);
      iPhysics.addParticle(p);
    }

    VerletParticle2D endWire  = new VerletParticle2D(mEnd, particleStartHeight);
    endWire.lock();

    VerletParticle2D prev = iPhysics.particles.get(physics.particles.size()-1);
    // create a spring between the previous and the current particle of length 10 and strength 1
    VerletConstrainedSpring2D s = new VerletConstrainedSpring2D(endWire, prev, mSegmentLengths, mSegmentStrengths, mMaxSpringLength);
    // add the spring to the physics system
    iPhysics.addSpring(s);
    iPhysics.addParticle(endWire);

    //Add constraints for the poles. Need to check the width of the image to make sure that the widths are correct 
    
    mRect[0]=new VisibleRectConstraint(new Vec2D(mFirstFixedPosition, mTop), new Vec2D(mFirstFixedPosition + poleWidth, iGroundHeight ));
    mRect[1]=new VisibleRectConstraint(new Vec2D(mSecondFixedPosition, mTop), new Vec2D(mSecondFixedPosition + poleWidth, iGroundHeight ));
    for (int i=0; i<mRect.length; i++) {
      VerletPhysics2D.addConstraintToAll(mRect[i], iPhysics.particles);
    }

    //The constraint to stop the rope going through the ground.
    //mGroundConstraint = new RectConstraint(new Vec2D(0.0, iGroundHeight - 10.0), new Vec2D(width, iGroundHeight + poleHeight));
    //VerletPhysics2D.addConstraintToAll(mGroundConstraint, iPhysics.particles);
  }

  //Add a weight to the clothesline to the particle closest to iPosition. The user needs to have clicked within mAttachRadius to have the object attach to it.
  boolean addWeight(VerletPhysics2D iPhysics, float iWeight, float iHeight, Vec2D iClickPosition, Ellipse iEllipse, ArrayList<Weight> iWeights) {

    //Not allowed to add items at the poles.
    for (int i=0; i< mRect.length; i++) {
      float[] extents = getPolePositions(i);
      if (iClickPosition.x >= extents[0] && iClickPosition.x <= extents[1])
        return false;
    }

    // Find the closest particle to the click position
    float closestParticleSqrd = mAttachRadius*mAttachRadius;
    VerletParticle2D closestParticle = iPhysics.particles.get(0);
    int closestParticleIndex = 0;

    for (int i=0; i< iPhysics.particles.size(); i++) {
      VerletParticle2D p = iPhysics.particles.get(i);
      Vec2D position = p.getPreviousPosition();
      float distanceSquared = position.distanceToSquared(iClickPosition);
      //Ignore if the closest particle is one of the locked ones.
      if (distanceSquared < closestParticleSqrd  && !p.isLocked()) {
        closestParticleSqrd = distanceSquared;
        closestParticle = p;
        closestParticleIndex = i;
      }
    }

    //The user clicked close enough to add the weight to the clothesline only if it does not collide with any other ellipses.
    float ellipseHalfWidth = iWeight*0.5;
    Vec2D particlePos = closestParticle.getPreviousPosition();
    if (closestParticleSqrd < mAttachRadius*mAttachRadius && !Util.isOverlap(particlePos, ellipseHalfWidth, iWeights)) {
      iEllipse.setParticleIndex(closestParticleIndex);
      iEllipse.setPosition(particlePos);
      float ellipseHalfHeight = iHeight*0.5;
      iEllipse.setRadii(ellipseHalfWidth, ellipseHalfHeight);
      return true;
    }

    return false;
  }

  //Add the forces to the particle each draw routine because an added force is only included for a single phyics.update().
  void applyForce(VerletPhysics2D iPhysics, Weight iWeight) {
    VerletParticle2D p = iPhysics.particles.get(iWeight.mEllipse.mParticleIndex);
    p.addForce(iWeight.mWeightForce);
  }

  //Draw the clothesline by drawing a line to represent each spring.
  void display(VerletPhysics2D iPhysics) {
    // draw a line segment for each spring and change the color of it based on the x position
    for (VerletSpring2D s : iPhysics.springs) {
      //OLD
      // map the direction of each spring to a hue
      //float currHue=map(s.b.sub(s.a).heading(),-PI,PI,0,1);
      //// define a color in HSV and convert into ARGB format (32bit packed integer)
      //stroke(TColor.newHSV(currHue,1,1).toARGB());
      //stroke(0);
      //line(s.a.x,s.a.y,s.b.x,s.b.y);

      noStroke();
      float ySize = 16.0;
      beginShape(QUAD);
      texture(mRopeTexture);
      vertex(s.a.x, s.a.y, 0, 0);
      vertex(s.b.x, s.b.y, ySize, 0);
      vertex(s.b.x, s.b.y+ySize, ySize, ySize);
      vertex(s.a.x, s.a.y+ySize, 0, ySize);
      endShape();
    }

    for (int i=0; i<mRect.length; i++) {
      mRect[i].display(mPoleImage);
      //mRect[i].draw();
    }
  }

  //Get the left and right x positions of pole i (rectangular constraint i), currently i = 0 or 1. 
  float[] getPolePositions(int iIndex) {
    float[] positions = {0.0, 0.0}; 
    if (iIndex >= mRect.length)
      return positions;

    Rect pole = mRect[iIndex].getBox();
    positions[0] = pole.getLeft();
    positions[1] = pole.getRight();
    return positions;
  }
}