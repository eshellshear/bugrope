//A class to store all the data for the catcher used in the CatchingGame to swat bugs

class Catcher extends Ellipse {

  //Variables
  //We continously cycle through the images. 
  ArrayList<PImage> mImages;
  int mCurrentImage;
  final int mStuckIndex = 5;
  //The number of images which make up the catchers motion.
  final int mNumberOfImages = 6;
  final int mFramesStuck = 90;
  int mUnstuckFrame;
  boolean mCatcherStuck;
  boolean mJustStuck;
  boolean mJumping;
  int mNotMoved;
  int mDirection;
  final float mExtraJumpHeight = 2.0;

  //Constructor
  Catcher() {
    super();
    mImages = new ArrayList<PImage>();
    mCurrentImage = 0;
    mCatcherStuck = false;
    mJustStuck = false;
    mDirection = 1;
    mJumping = false;
    mNotMoved = 0;
    mUnstuckFrame = 0;
  }

  Catcher(Ellipse iEllipse, ArrayList<PImage> iImages) {
    super(iEllipse);
    mImages = iImages;
    mCurrentImage = 0;
    mCatcherStuck = false;
    mJustStuck = false;
    mJumping = false;
    mNotMoved = 0;
    mDirection = 1;
    mUnstuckFrame = 0;
  }
  
  Catcher(Catcher iOther) {
    super(iOther.mPosition, iOther.mRadii, iOther.mParticleIndex);
    mImages = iOther.mImages;
    mCurrentImage = iOther.mCurrentImage;
    mCatcherStuck = iOther.mCatcherStuck;
    mJustStuck = iOther.mJustStuck;
    mDirection = iOther.mDirection;
    mUnstuckFrame = iOther.mUnstuckFrame;
    mJumping = iOther.mJumping;
    mNotMoved = iOther.mNotMoved;
  }

  //Methods
  void moveLeft(float iFrameCount) {
    if(iFrameCount %2 ==0)
      moveLeft();
    
  }

  void moveRight(float iFrameCount) {
    if(iFrameCount %2 ==0)
      moveRight();
  }
  
  void moveLeft() {
    if(!mJumping && super.mPosition.x > super.mRadii.x)
      super.mPosition.x -= super.mRadii.x*0.5;
    
    mNotMoved = 0;
    
  }

  void moveRight() {
    if(!mJumping && super.mPosition.x < width - super.mRadii.x)
      super.mPosition.x += super.mRadii.x*0.5;
      
    mNotMoved = 0;
  }
  
  void setEllipse(Ellipse iEllipse){
    mPosition = iEllipse.mPosition;
    mRadii = iEllipse.mRadii;
  }
  
  void resetPosition(float iResetPosition){
   mPosition.x = iResetPosition;
  }
  
  float getDistSquared(Vec2D iPos){
    return mPosition.distanceToSquared(iPos);
  }
  
  void setData(float iRX, float iRY, float iPosX, float iPosY){
    mRadii.x = iRX;
    mRadii.y = iRY;
    mPosition.x = iPosX;
    mPosition.y = iPosY;
  }

  int getNumberOfImages() {
    return mNumberOfImages;
  }

  boolean isStuck() {
    return mCatcherStuck;
  }

  void setStuck(boolean iStuck) {
    mCatcherStuck = iStuck;
  }

  PImage getImage() {
    return mImages.get(0);
  }

  //Methods
  void addImage(String iPath) {
    PImage image;
    image = loadImage(iPath);
    mImages.add(image);
  }

  void display(float iFrameCount) {
    Vec2D topLeft = getTopLeft();
    if (mCatcherStuck) {
      image(mImages.get(mStuckIndex), topLeft.x, topLeft.y);
      //Set unstuck after a while.
      if(!mJustStuck){
        mUnstuckFrame = (int)iFrameCount + mFramesStuck;
        mJustStuck = true;
      }
      
      if((int)iFrameCount > mUnstuckFrame){
        mCatcherStuck = false;
        mJustStuck = false;
        mCurrentImage= 0;
        mDirection = 1;
      }
    } else {
      //If the player hasn't moved it then I make it jump.
      if(mCurrentImage ==  2 && mNotMoved >= 5){
        mJumping = true;
        mPosition.y -= mRadii.y*mExtraJumpHeight;
        topLeft = getTopLeft();
        mNotMoved = 0;
        image(mImages.get(mCurrentImage), topLeft.x, topLeft.y);
        return;
      }
      
      if(!mJumping){
        image(mImages.get(mCurrentImage), topLeft.x, topLeft.y);
        if(iFrameCount % 5 == 0){
          ++mNotMoved;
          //super.mPosition.x += ((float)mCurrentImage - 2.0)*super.mRadii.x/10.0;
          mCurrentImage += mDirection;
        }
        
        if((mCurrentImage == mStuckIndex-1 && mDirection == 1) || (mCurrentImage == 0 && mDirection == -1)){
          mDirection *= -1;
        }
      }else{
        image(mImages.get(mCurrentImage), topLeft.x, topLeft.y);
        if(iFrameCount % 20 == 0){
          mJumping = false;
          mPosition.y += mRadii.y*mExtraJumpHeight;
          topLeft = getTopLeft();
        }
      }
    }
  }
}