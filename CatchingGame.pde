//In this game you move a swatter along the "ground". You catch a hanging bug if you touch it. 

class CatchingGame {
  //Class Variables
  Catcher mCatcher;
  float mGroundHeight;
  ArrayList<Bug> mBugs;
  ArrayList<SquashedBug> mSquashedBugs;
  ArrayList<Obstacle> mObstacles;
  int mNumberOfLives;
  //The bug showing the number of lives left.
  ArrayList<LifeBug> mLifeBugs;

  //Constructor
  CatchingGame(float iGroundHeight, Catcher iCatcher) {
    mCatcher = new Catcher(iCatcher);
    mBugs = new ArrayList<Bug>();
    mSquashedBugs = new ArrayList<SquashedBug>();
    mObstacles = new ArrayList<Obstacle>();
    mLifeBugs = new ArrayList<LifeBug>();
    mGroundHeight = iGroundHeight;
    mNumberOfLives = 3;
    //Just choose an arbitrary bunch of values of size, etc. The size should be based on the size of the picture. This is done later.
    mCatcher.setData(50.0, 50.0, width*0.5, mGroundHeight - mCatcher.mRadii.y*0.6);
  }
  
  //Methods
  void clearAll(){
    mBugs.clear();
    mSquashedBugs.clear();
    mObstacles.clear();
    mNumberOfLives = 3;
    for(LifeBug l: mLifeBugs)
      l.setIsDead(false);
    
    mCatcher.resetPosition(width*0.5);
  }

  void addBug(Bug iBug) {
    mBugs.add(iBug);
  }
  
  void addLifeBug(LifeBug iBug){
    mLifeBugs.add(iBug); 
  }

  void addObstacle(Obstacle iObstacle) {
    mObstacles.add(iObstacle);
  }
  
  boolean timeForNewItem(float iFrameRate, int iCurrentLevel){
    if(iCurrentLevel < 2)
      return iFrameRate % 40 == 0;
    
    if(iCurrentLevel < 4)
      return iFrameRate % 35 == 0;
    
    if(iCurrentLevel < 6)
      return iFrameRate % 30 == 0;
    
    if(iCurrentLevel < 8)
      return iFrameRate % 25 == 0;
    
    if(iCurrentLevel < 10)
      return iFrameRate % 20 == 0;  
      
    if(iCurrentLevel < 12)
      return iFrameRate % 15 == 0;
      
    //This is the fastest it will get after level 12.
    return iFrameRate % 10 == 0;
  }
  
  boolean isCatcherClose(Bug iBug) {
    float dangerThreshold = 1.3;
    float distanceSqrd = mCatcher.getDistSquared(iBug.getPosition());
    if (distanceSqrd < sq(iBug.mEllipse.getEllipseSize()*dangerThreshold + mCatcher.getEllipseSize()*dangerThreshold)) {
      return true;
    }

    return false;
  }

  boolean caughtBug(Bug iBug) {
    //Compute distance between catcher and bug.
    float distanceSqrd = mCatcher.mPosition.distanceToSquared(iBug.mEllipse.mPosition);
    if (distanceSqrd < sq(iBug.mEllipse.getEllipseSize() + mCatcher.getEllipseSize())) {
      return true;
    }
    return false;
  }
  
  boolean gainLife(){
    if(mNumberOfLives == 3)
      return false;
    
    ++mNumberOfLives;
    LifeBug lifeBug = mLifeBugs.get(2);
    int lifeBugIndex = 1;
    while(!lifeBug.isDead() && lifeBugIndex >= 0){
      lifeBug = mLifeBugs.get(lifeBugIndex);
      --lifeBugIndex;
    }
    lifeBug.setIsDead(false);
    return true;
  }
  
  //Draw all the bugs
  boolean display(GameAssets iAssets, float iFrameCount, Constants iConstants, VerletPhysics2D iPhysics) {
    boolean gameOK = true;
    //Display number of player lives
    for(LifeBug l: mLifeBugs){
      l.display();
    }
    
    for (int i =0; i<mBugs.size(); ++i) {
      Bug b = mBugs.get(i);
      b.mEllipse.updatePosition(iPhysics, mGroundHeight);
      if (!mCatcher.isStuck() && caughtBug(b)) {
        iConstants.GAME_SCORE += b.mEllipse.getAvgSize();
        //Add a new squished bug to the list and remove the current bug
        Vec2D topLeft = b.mEllipse.getTopLeft();
        SquashedBug squashedBug = new SquashedBug(topLeft, iAssets.mSquashedBug.mImages);
        mSquashedBugs.add(squashedBug);
        
        Util.playSound(iAssets.mSquashedBugSound);
        //Do a replace with back to avoid O(n) removals.
        Util.replaceWithBack(mBugs, i);

        //Decrement i so that we don't skip anything.
        i--;
      } else if (isCatcherClose(b)) {
        //If the catcher is close to the bug make it move scared.
        b.displayScared();
      } else {
        b.displaySimple();
      }
    }

    if (mObstacles.isEmpty()) {
      //The catcher keeps track of it is current state, stuck or bombed, and displays accordingly.
      mCatcher.display(iFrameCount);
    } else {
      //Check if the catcher is near an obstacle. If it hits a bomb, game over. If it hits a spider web it is stuck for two seconds or so.
      for (int i=0; i < mObstacles.size(); ++i) {
        Obstacle o = mObstacles.get(i);
        if(o.isBomb()){
          Bomb bomb = (Bomb)o;
          if(!bomb.isExploded()){
            o.mEllipse.updatePosition(iPhysics, mGroundHeight); 
          }
        }else{
          o.mEllipse.updatePosition(iPhysics, mGroundHeight);
        }
        if (!o.isCaught() && o.gotCatcher(mCatcher)) {
          //If bomb then catcher is dead. If spider web then catcher is stuck.
          if (o.isBomb()) {
            
              LifeBug lifeBug = mLifeBugs.get(0);
              int lifeBugIndex = 1;
              while(lifeBug.isDead() && lifeBugIndex < mLifeBugs.size()){
                lifeBug = mLifeBugs.get(lifeBugIndex);
                ++lifeBugIndex;
              }
              lifeBug.setIsDead(true);
              mNumberOfLives--;
              if(mNumberOfLives <= 0){
                gameOK = false;
              }
              
              Util.playSound(iAssets.mExplosion);
          } else {
            //It was a spider web so make it stuck.
            o.setCaught(true);
            mCatcher.setStuck(true);
          }
        }
        
        if(o.isFinished()){
          Util.replaceWithBack(mObstacles, i);
        }else{
          o.display(iFrameCount);
        }
      }
      mCatcher.display(iFrameCount);
    }

    //Once they are finished, stop displaying them.
    for (int i=0; i < mSquashedBugs.size(); ++i) {
      SquashedBug b = mSquashedBugs.get(i);
      if (!b.isFinished()) {
        b.display(frameCount);
      } else {
        Util.replaceWithBack(mSquashedBugs, i);
      }
    }

    return gameOK;
  }
}