# README #

Read the following to understand the licensing and running of the BugRope game. 

### What is this repository for? ###

This repository is for all the files and data for the BugRope game. It is currently available on Google Play at https://play.google.com/store/apps/details?id=processing.test.bugrope&hl=en.

### How do I get set up? ###

To get BugRope up and running you will need to install the processing development environment (PDE) which can be gotten at http://www.processing.org/. You will also need to install the toxiclibs library for the physics simulation.

Finally currently the code is written for Android but it is very simple to change that for Windows/Mac by swapping out all the Android commands with normal java ones and also replacing all the Android music/sounds functions with the minim library for processing.

### Contribution guidelines ###

If you wish to contribute something just send me a message. I am more than happy to add improvements to the code. 

### Licensing ###

I am releasing all my code under the MIT license. This means you can use it for whatever you want. I do not provide any guarantees or anything with my code. You use it at your own risk!

### Support ###

The only person maintaining this repository is me, Evan Shellshear. It is probably too much for me to answer all questions and requests so please just give things a go. If you break it, it doesn't matter. It is only code!