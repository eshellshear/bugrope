//A class to store all the data for each of the bugs.

class Bug extends Weight {

  //Variables
  //Image 1 is the default image. The other two are to simulate a bit of motion.
  ArrayList<PImage> mImages;
  int mCurrentImage;
  final int mNumberOfImages = 3;

  //Constructor
  Bug() {
    super();
    mImages = new ArrayList<PImage>();
    mCurrentImage = 0;
  }

  Bug(Vec2D iWeight, Ellipse iEllipse, ArrayList<PImage> iImages) {
    super(iWeight, iEllipse);
    mImages = iImages;
    mCurrentImage = 0;
  }

  //Methods
  int getNumberOfImages() {
    return mNumberOfImages;
  }

  //Methods
  void addImage(String iPath) {
    PImage image;
    image = loadImage(iPath);
    mImages.add(image);
  }
  
  float getAvgSize(){
    return mEllipse.getAvgSize(); 
  }
  
  Vec2D getPosition(){
    return mEllipse.mPosition; 
  }

  Vec2D getWeightForce() {
    return super.getWeightForce();
  }
  
  float getWidth(){
    return getBugLook().width;
  }
  
  float getHeight(){
   return getBugLook().height; 
  }

  PImage getBugLook() {
    return getBugLook(1);
  }

  PImage getBugLook(int iIndex) {
    return mImages.get(iIndex); //<>//
  }

  //Get the default normal image
  void displaySimple() {
    Vec2D topLeft = super.mEllipse.getTopLeft();
    image(getBugLook(), topLeft.x, topLeft.y);
   }

  //Cycle through the different images.
  void displayScared() {
    Vec2D topLeft = super.mEllipse.getTopLeft();
    image(getBugLook(mCurrentImage), topLeft.x, topLeft.y);
    ++mCurrentImage;
    mCurrentImage = mCurrentImage % mImages.size();
  }
}