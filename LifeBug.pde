//The class to display the image of the number of lives a player has

class LifeBug{

  //The picture
  PImage mImage;
  //It's location
  Vec2D mPosition;
  //Whether this life is still valid or not.
  boolean mIsDead;

  //Constructor
  LifeBug() {
    mPosition = new Vec2D();
    mIsDead = false;
  }
  
  LifeBug(Vec2D iPosition, PImage iImage) {
    mPosition = iPosition;
    mImage = iImage;
    mIsDead = false;
  }

  //Methods
  void setIsDead(boolean iDead){
    mIsDead = iDead;
  }
  
  boolean isDead(){
   return mIsDead; 
  }
  
  void setPosition(Vec2D iPosition){
    mPosition = iPosition; 
  }
  
  PImage getImage() {
    return mImage;
  }
  
  float getWidth(){
   return mImage.width; 
  }
  
  float getHeight(){
   return mImage.height; 
  }
  
  PImage getLook() {
    return mImage;
  }
  
  //Get top left of image.
  Vec2D topLeft(){
    return mPosition; 
  }
  
  //Get bottom right of image.
  Vec2D bottomRight(){
    return new Vec2D(mPosition.x + getWidth(), mPosition.y + getHeight()); 
  }

  
  void addImage(String iPath) {
    mImage = loadImage(iPath);
  }

  
  void display() {
    image(mImage, mPosition.x, mPosition.y);
    //Draw a red cross through it if this life is lost.
    if(mIsDead){
      Vec2D topLeft = topLeft();
      Vec2D bottomRight = bottomRight();
      
      stroke(250, 10, 0);
      line(topLeft.x , topLeft.y, bottomRight.x, bottomRight.y);
      line(topLeft.x , bottomRight.y, bottomRight.x, topLeft.y);
      
    }
    
  }
}