//In this game you have to try and hang as many weights as possible without letting any touch the "ground"You lose if any touch the ground.

class HangingGame {
  //Variables
  float mGroundHeight;
  //We need to keep local copies of each of the assets so that each can act independently
  ArrayList<Bug> mBugs;
  ArrayList<SquashedBug> mSquashedBugs;
  //It probably makes sense to give the player 3 lives or something. And to count down the lives and then when none are left return false in display function.
  int mNumberOfLives;
  //The bug showing the number of lives left.
  ArrayList<LifeBug> mLifeBugs;
  
  //All the bonus variables
  //The bonus text
  String mBonusText;
  //The length of time to display the text in milliseconds
  final int mBonusDisplayLength = 5000;
  //The time point to stop displaying
  int mStopBonusDisplay;
  
  //Constructor 
  HangingGame(float iGroundHeight) {
    mGroundHeight = iGroundHeight;
    mBugs = new ArrayList<Bug>();
    mSquashedBugs = new ArrayList<SquashedBug>();
    mLifeBugs = new ArrayList<LifeBug>();
    mNumberOfLives = 3;
    mBonusText = new String("Filled rope 100 Bonus points!");
  
  }

  //Methods
  //If a mouse button has been pressed carry out the following action
  boolean mouseAction(Clothesline iClothesline, VerletPhysics2D iPhysics, float iNextWeight, float iNextHeight, Vec2D iMouseLoc, Ellipse iEllipse, ArrayList<Weight> iWeights) {
    return iClothesline.addWeight(iPhysics, iNextWeight, iNextHeight, iMouseLoc, iEllipse, iWeights);
  }
  
  void clearAll(boolean iClearLifeBugs){
    mBugs.clear();
    mSquashedBugs.clear();
    if(iClearLifeBugs){
      mNumberOfLives = 3;
      for(LifeBug l: mLifeBugs)
        l.setIsDead(false);
    }
   }
   
   void displayBonus(){
     mStopBonusDisplay = millis() + mBonusDisplayLength;
   }
   
  //Check there is a gap big enough to fit this object. Not allowed to hang up items on the clothesline poles!
  boolean enoughSpaceLeft(Clothesline iClothesline, float iWidth) {
    if (mBugs.isEmpty())
      return true;

    //Create a list of all maximum and minimum extents of all objects. +2 for distances to screen edges.
    float[] extents = new float[2*mBugs.size() + 2*iClothesline.mRect.length + 2]; 
    //Add first extent at beginning of screen.
    extents[0] = 0.0;

    //Add one to each weight index because we start from 1.  
    for (int i=0; i< mBugs.size(); ++i) {
      extents[2*i+1]= mBugs.get(i).mEllipse.getEllipseLeft();
      extents[2*i+2]= mBugs.get(i).mEllipse.getEllipseRight();
    }

    //Add one because of the initial 0.0.
    int counter = 2*mBugs.size() + 1;
    for (int i=0; i< iClothesline.mRect.length; ++i) {
      float[] poleExtents = iClothesline.getPolePositions(i);
      extents[counter + 2*i]= poleExtents[0];
      extents[counter + 2*i+1]= poleExtents[1];
    }

    //Add final extent to edge of screen.
    extents[extents.length-1]= width;
    float[] sortedExtents = sort(extents);

    float biggestGap = extents[0];
    for (int i=1; i < sortedExtents.length; i=i+2) {
      float gap = sortedExtents[i] - sortedExtents[i-1];
      if (gap > biggestGap) {
        biggestGap = gap;
      }
    }

    //Slightly shrink the biggest gap to make sure it's not too difficult to place the last piece.
    float approximationFactor = 0.7;
    if (iWidth < biggestGap*approximationFactor )
      return true;

    return false;
  }

  boolean isCloseToGround(Bug iBug) {
    float dangerThreshold = 0.9;
    if (iBug.mEllipse.getEllipseLowest() + iBug.mEllipse.getEllipseSize()*dangerThreshold > constants.GROUND_HEIGHT) {
      return true;
    }

    return false;
  }

  void addBug(Bug iBug) {
    mBugs.add(iBug);
  }
  
  void addLifeBug(LifeBug iBug){
    mLifeBugs.add(iBug); 
  }

  //Draw all the bugs
  boolean display(GameAssets iAssets, VerletPhysics2D iPhysics) {
    if(millis() < mStopBonusDisplay){
      fill(165,82,8);
      float textLength = 60.0;
      textAlign(CENTER);
      text(mBonusText, width/2 - textLength, height/2 + constants.FONT_SIZE );
    }
    
    for(LifeBug l: mLifeBugs){
      l.display();
    }
    
    boolean gameOK = true;
    for (int i=0; i< mBugs.size(); ++i) {
      Bug b = mBugs.get(i);
      b.mEllipse.updatePosition(iPhysics, mGroundHeight);
      //If the bug touches the ground it gets squashed.
      if (b.mEllipse.getEllipseLowest() > constants.GROUND_HEIGHT) {
        //Take a life off the player
        LifeBug lifeBug = mLifeBugs.get(0);
        int lifeBugIndex = 1;
        while(lifeBug.isDead() && lifeBugIndex < mLifeBugs.size()){
          lifeBug = mLifeBugs.get(lifeBugIndex);
          ++lifeBugIndex;
        }
        lifeBug.setIsDead(true);
       
        //The order here is important: add the squashed bug before removing it!
        //Add a new squashed bug to the list and remove the current bug 
        Vec2D topLeft = b.mEllipse.getTopLeft();
        SquashedBug squashedBug = new SquashedBug(topLeft, iAssets.mSquashedBug.mImages);
        mSquashedBugs.add(squashedBug);
        Util.playSound(iAssets.mSquashedBugSound);
                
        //Implement a replace with back to avoid O(n) removals.
        Util.replaceWithBack(mBugs, i);
        //Decrement i so that we don't skip anything.
        i--;
        
        mNumberOfLives--;
        if(mNumberOfLives <= 0)
          gameOK = false;
        
      } else if (isCloseToGround(b)) {
        //If the bug is close to the ground we make it move scared.
        b.displayScared();
      } else {
        b.displaySimple();
      }
    }
    
    //Once they are finished, stop displaying them.
    for (int i=0; i < mSquashedBugs.size(); ++i) {
      SquashedBug b = mSquashedBugs.get(i);
      if (!b.isFinished()) {
        b.display(frameCount);
      } else {
        Util.replaceWithBack(mSquashedBugs, i);
      }
    }

    return gameOK;
  }
}