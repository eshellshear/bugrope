import toxi.geom.*;

//A weight class for other classes to inherit from. 

class Weight {
  //Variables
  Vec2D mWeightForce; //the 2D weight of the weight (including gravity)
  Ellipse mEllipse; //the ellipse representing the form of the weight.

  //Constructors
  Weight() {
    mWeightForce = new Vec2D(0.0, 0.0);
    mEllipse = new Ellipse();
  }

  Weight(Vec2D iWeight, Ellipse iEllipse) {
    mWeightForce = iWeight.copy();
    mEllipse = iEllipse;
  }

  //Methods
  void setWeightForce(Vec2D iWeightForce) {
    mWeightForce = iWeightForce;
  }

  Vec2D getWeightForce() {
    return mWeightForce;
  }

  void setEllipse(Ellipse iEllipse) {
    mEllipse = iEllipse;
  }
}