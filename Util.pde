//A bunch of general functions to avoid repeating code

static class Util {
  
  //A replace with back function to avoid deleting elements in an array. Changes an O(n) function into O(1).
  static <V> void replaceWithBack(ArrayList<V> iA, int iIndex) {
    if (iA.size() == 1) {
      iA.clear();
      return;
    }

    iA.set(iIndex, iA.get(iA.size()-1));
    iA.remove(iA.size()-1);
  }

  //Check for overlap between the hanging items. HACK: This is not the correct distance between things. It only takes the x direction into account so it is conservative.
  static boolean isOverlap(Vec2D iParticlePos, float iWidth, ArrayList<Weight> iWeights) {
    Vec2D newExtent = new Vec2D(iParticlePos.x - iWidth, iParticlePos.x + iWidth);
    for (Weight w : iWeights) {
      Vec2D otherExtent = new Vec2D(w.mEllipse.mPosition.x - w.mEllipse.mRadii.x, w.mEllipse.mPosition.x + w.mEllipse.mRadii.x);
      //HACK: Can add an approximation factor to allow things to get closer.
      if (otherExtent.y >=  newExtent.x && otherExtent.x <=  newExtent.y) {
        return true;
      }
    }
    return false;
  }
  
  //A function to play a sound. 
  static void playSound(MediaPlayer iPlayer){
    if(!iPlayer.isPlaying())
     iPlayer.start();
  }
 
}