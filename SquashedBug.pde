//Creates the visualization for a squashed bug. There are 4 images to visualize a bug when it gets squashed.

class SquashedBug {
  //Variables
  Vec2D mPosition;
  ArrayList<PImage> mImages;
  int mCurrentImage;
  final int mNumberOfImages = 4;
    
  //Constructor
  SquashedBug(){
    mImages = new ArrayList<PImage>();
    mCurrentImage = 0;
    mPosition = new Vec2D();
  }
  
  SquashedBug(Vec2D iPosition, ArrayList<PImage> iImages) {
    mImages = iImages;
    mCurrentImage = 0;
    mPosition = iPosition;
  }

  //Methods
  int getNumberOfImages() {
    return mNumberOfImages;
  }

  
  void addImage(String iPath) {
    PImage image;
    image = loadImage(iPath);
    mImages.add(image);
  }

  boolean isFinished() {
    return  mCurrentImage == mImages.size();
  }

  void display(float iFrameCount) {
    //Display image
    image(mImages.get(mCurrentImage), mPosition.x, mPosition.y);
    if(iFrameCount % 5 == 0){
      ++mCurrentImage;
    }
    //mCurrentImage = mCurrentImage % mImages.size();
  }
}