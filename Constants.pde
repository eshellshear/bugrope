//A class storing all game constants



class Constants{
  //Variables
  //The number of particles in the rope
  int NUM_PARTICLES;
  //The gravity force vector
  final Vec2D GRAVITY = new Vec2D(0.0, 0.15);
  //The height of the ground 
  float GROUND_HEIGHT;
  //The current game score
  int GAME_SCORE;
  //The final game score
  int FINAL_SCORE;
  //The maximum number of obstacles allowed in the catching game
  final int MAX_NUMBER_OF_OBSTACLES = 4;
  //The position of the score along the x axis
  float SCORE_XPOSITION;
  //The position of the level info along the x axis
  float LEVEL_XPOSITION;
  //The position of the next bug image along the x axis
  float NEXT_BUG_XPOSITION;
  //The starting position of the next word text next to the bug
  float NEXT_XPOSITION;
  //A boolean to say whether the game is over or not
  boolean GAME_OVER = false;
  //The number of types of bugs
  final int NUMBER_OF_BUG_TYPES = 5;
  //The number of types of obstacles
  final int NUMBER_OF_OBSTACLE_TYPES = 2;
  //Need to figure out a way to always save these. So when the game opens they are there. 
  //Also need a way to clear them if the user wants to. Have at most five highest scores.
  int[] HIGHEST_SCORES = {0,0,0,0,0};
  //A boolean to say when you go up a level
  boolean LEVEL_UP = false;
  //A PFont object to store the font for game text
  PFont ROPE_FONT;
  //A Pfont object to store the font for smaller game text
  PFont GAME_ROPE_FONT;
  //A int to store the size of the larger text
  int FONT_SIZE;
  //An inte to store the size of the smaller text
  int GAME_FONT_SIZE;
  //Number of pixels displacement from top all things are.
  final int TOP_DISPL = 20;
  //The length of time to display the level up text in milliseconds
  final int mLevelUpDisplayLength = 5000;
  //The time point to stop displaying
  int mStopLevelUpDisplay;
  //The text to display when you level up
  String mLevelUpTxt;
  //The width of the screen
  float mScreenWidth;
  //The height of the screen
  float mScreenHeight;
  //The level text at the top of the screen
  String mLevelText;
  //The score text at the top of the screen
  String mScoreText;
  //The next bug text
  String mNextBugText;
  //The top strip in game mode which is removed
  float mBannerHeight; 
  
    
  //Constructor
  Constants(float iWidth, float iHeight, String iRopeFont){
     GAME_SCORE= 0;
     FINAL_SCORE = 0;
     NUM_PARTICLES = 150;
     mLevelUpTxt = new String("Level Up! Level ");
     mLevelText  = new String("Level ");
     mScoreText  = new String("Score ");
     mNextBugText = new String("Next ");
     mScreenWidth = iWidth;
     mScreenHeight = iHeight;
     GROUND_HEIGHT = mScreenHeight;
     FONT_SIZE = (int)(iHeight/13.0);
     GAME_FONT_SIZE = (int)(FONT_SIZE*0.9);
     ROPE_FONT = createFont(iRopeFont, FONT_SIZE);
     GAME_ROPE_FONT = createFont(iRopeFont, GAME_FONT_SIZE);
     setNumParticles();
  }
  
  //Methods
  void setBannerHeight(float iBackButtonHeight){
    float extraHeight = iBackButtonHeight/3;
    mBannerHeight = iBackButtonHeight + extraHeight;
  }
  
  int getBannerHeight(){
    return (int)mBannerHeight;
  }
  
  boolean updateHighScores(int iScore){
    boolean newHighScore = false;
    //Sorts from smallest to largest 
    HIGHEST_SCORES = sort(HIGHEST_SCORES);
    //Reverse the list so we have largest to smallest
    HIGHEST_SCORES = reverse(HIGHEST_SCORES);
    int score = iScore;
    for(int i=0; i < HIGHEST_SCORES.length; ++i){
      if(score > HIGHEST_SCORES[i]){
       int tmpScore = HIGHEST_SCORES[i];
       HIGHEST_SCORES[i] = score;
       score = tmpScore;
       newHighScore = true;
      }
    }
    return newHighScore;
  }
  
  //Set the positions of all the texts at the top of the screen
  void setBannerText(float iLifeBugWidth){
    float startX = iLifeBugWidth *1.1;
    LEVEL_XPOSITION = (int)startX + mLevelText.length()*GAME_FONT_SIZE*0.5;
    SCORE_XPOSITION = LEVEL_XPOSITION + (mLevelText.length())*GAME_FONT_SIZE;
    NEXT_XPOSITION = SCORE_XPOSITION + (mScoreText.length())*GAME_FONT_SIZE*0.8;
    NEXT_BUG_XPOSITION = NEXT_XPOSITION + (mNextBugText.length())*GAME_FONT_SIZE*0.3;
  }
  
  //Set the number of particles in the rope. The following works well with 1280 and 1920 resolutions.
  void setNumParticles(){
    if(width < 1300){
      NUM_PARTICLES = (int)map(mScreenWidth, 0.0, 7680.0, 0.0, 693.0);
    }else if(width < 2100){
      NUM_PARTICLES = (int)map(mScreenWidth, 0.0, 7680.0, 0.0, 685.0);
    }else{
      NUM_PARTICLES = (int)map(mScreenWidth, 0.0, 7680.0, 0.0, 642.0);
    }
  }
  
  void reset(){
    GAME_OVER = false;
    GAME_SCORE = 0;
    FINAL_SCORE = 0;
    
  }
}