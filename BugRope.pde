//NOTE: The following code is for compiling in Android mode of processing. To get it to work for Windows/Mac remove all Android code and replace the sound stuff with the Minim library. 

//This is the main file for the BugRope game.

import java.util.Iterator;
import toxi.physics2d.*;
import toxi.geom.*;
import toxi.physics2d.constraints.RectConstraint;

import android.media.MediaPlayer;
import android.content.res.AssetFileDescriptor;
import android.content.Context;
import android.app.Activity;

import android.os.Bundle;
import android.widget.ImageView;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.BufferedInputStream;
  
//Global Variables
//A variable to store the physics
VerletPhysics2D physics;
//Data to store the rope on which the bugs hang
Clothesline clothesline;
//The index of the next item (bug or obstacle)
int nextItemIndex;
//The data for the hanging game
HangingGame hangingGame;
//The data for the catching game
CatchingGame catchingGame;
//All the game assets data
GameAssets assets;
//A class to load all images from file
ImageLoader imgLoader;
//A class storing all the game constants
Constants constants;
//A class to store the intro buttons on the intro screen
IntroButtons introButtons;
//A boolean to control whether we initialize the phyics or not.
boolean firstTime;
//Android variables
Activity activity;
Context context; 
//A boolean to say whether the player has earned an extra life.   
boolean levelUpLife = false;

//A set of enums to know what screen to display
public enum GameType {
  INTRO, HANGING_GAME, CATCHING_GAME, HIGH_SCORE, CREDITS, HELP
}

//The starting screen when the game starts.
GameType gameType = GameType.INTRO;

//Program start.
void setup() {
  activity = this.getActivity();  
  context = activity.getApplicationContext();
  constants = new Constants(width, height, "AlphaRope.ttf");
  //Fullscreen for tablets and mobiles. 1280 for computers.
  orientation(LANDSCAPE);
  //size(1280,720,P2D);
  fullScreen(P2D);
  assets = new GameAssets(gameType);
  imgLoader = new ImageLoader(constants.GRAVITY, width);
  imgLoader.loadBackButton(assets.mBackButton);
  constants.setBannerHeight(assets.mBackButton.mImage.height);
  imgLoader.loadIntroImages(assets);
    
  image(assets.mBlankIntroScreen, 0,0);
  image(assets.mIntroScreen, 0,0);
  loadSounds();
  //Util.playSound(assets.mIntroSong);
  
  introButtons = new IntroButtons(constants.FONT_SIZE, height, width);
  fill(165,82,8);
  textFont(constants.ROPE_FONT);
    
  physics = new VerletPhysics2D();
  // add gravity in positive Y direction
  physics.addBehavior(new GravityBehavior(constants.GRAVITY));
  //// set the stroke weight of the line
  strokeWeight(2);
  clothesline = new Clothesline();
  clothesline.init(physics, constants.NUM_PARTICLES, constants.GROUND_HEIGHT);
    
  firstTime=true;  
}

void draw() {
  
  switch(gameType){
  case INTRO:
   introScreen();
   break;
  case HANGING_GAME:
    
   playGame();
   break;
  case CATCHING_GAME:
   
   playGame();
   break;
  case CREDITS:
    
   playCredits();
   break;
  case HIGH_SCORE:
    
   playHighScore();
   break;
  case HELP:
    
   playHelp();
   break;
  }
}

void introScreen(){
  //Load intro screen background and create four buttons.
  //image(assets.mIntroScreen, assets.mBackgroundPos.x,assets.mBackgroundPos.y);
  image(assets.mIntroScreen, 0, 0);
  introButtons.display();
  //Util.playSound(assets.mIntroSong);
  text("BUG ROPE", width/2, constants.FONT_SIZE);
  
  //Depending on what the user presses, this makes them go to that option next time.
  if(introButtons.pressHangButton()){
    background(255);
    text("LOADING", width/2, height/2);
    gameType = GameType.HANGING_GAME;
    //assets.mIntroSong.pause();
    Util.playSound(assets.mLevelUp);
    firstTime = true;
  }
  
  if(introButtons.pressCatchButton()){
    background(255);
    text("LOADING", width/2, height/2);
    gameType = GameType.CATCHING_GAME;
    //assets.mIntroSong.pause();
    Util.playSound(assets.mLevelUp);
    firstTime = true;
  }
  
  if(introButtons.pressScoreButton()){
    gameType = GameType.HIGH_SCORE;
    Util.playSound(assets.mLevelUp);
  }
  
  if(introButtons.pressCreditsButton()){
    gameType = GameType.CREDITS;
    Util.playSound(assets.mLevelUp);
  }
  
  if(introButtons.pressHelpButton()){
    gameType = GameType.HELP;
    Util.playSound(assets.mLevelUp);
  }
  
  assets.setGameType(gameType);
  introButtons.reset();
}

void playGame(){
  if(firstTime){
    initializePhysics();
    //Create the games.
    if(gameType == GameType.HANGING_GAME){
      hangingGame = new HangingGame(constants.GROUND_HEIGHT);
    }
    
    if(gameType == GameType.CATCHING_GAME){
      imgLoader.loadCatcher(assets.mCatcher);
      catchingGame = new CatchingGame(constants.GROUND_HEIGHT, assets.mCatcher);
    }
    
    //Load background image and bugs.
    loadGameImages();
    getNextItem();
    firstTime = false;
  }
  
  textAlign(CENTER);
  background(255);
  image(assets.mBackground, 0, constants.getBannerHeight());
  assets.mBackButton.display();
  if (!constants.GAME_OVER) {
    displayAllInfo(); 
    if(assets.levelUp(constants.GAME_SCORE)){
      constants.mStopLevelUpDisplay = millis() + constants.mLevelUpDisplayLength;
      //If the user goes up a level they win a life back!
      if(gameType == GameType.CATCHING_GAME){
        levelUpLife = catchingGame.gainLife();
      }   
    }
    
    if(millis() < constants.mStopLevelUpDisplay){
      fill(165,82,8);
      float textLength = 60.0;
      textAlign(CENTER);
      float extraSpace = 5.0;
      text("Next level at " + assets.mNextLevelScore, 2*width/3 - textLength, height/2);
      if(levelUpLife){
        text("Gained a life back",  2*width/3 - textLength, height/2 - extraSpace - constants.FONT_SIZE);
      }
    }else{
      levelUpLife = false;
    }
    
    if(gameType == GameType.CATCHING_GAME){
      playCatchingGame();
    }
      
    if(gameType == GameType.HANGING_GAME){
      playHangingGame();
    }
    
   }else{
    float extraSpace = 5.0;
    if (constants.GAME_SCORE !=0){
      constants.FINAL_SCORE = constants.GAME_SCORE;
      if(constants.updateHighScores(constants.GAME_SCORE)){
        text("New High Score " + constants.GAME_SCORE, width/2, height/2 + constants.FONT_SIZE + extraSpace);
      } 
    }
    constants.GAME_SCORE =0;
    fill(165,82,8);
    textAlign(CENTER);
    text("You lose", width/2, height/2 - constants.FONT_SIZE*0.5 - extraSpace);
    text("Your final score " + constants.FINAL_SCORE, width/2, height/2  - extraSpace  + constants.FONT_SIZE*0.5); 
    text("Touch the screen to continue", width/2, height/2 + 1.5*constants.FONT_SIZE);
   }
}

void playHangingGame(){
  for (Bug b : hangingGame.mBugs) {
    clothesline.applyForce(physics, b);
  }
  
  physics.update();
  clothesline.display(physics);
  
  if (!hangingGame.display(assets, physics)) {
    constants.GAME_OVER = true;
  }
}

void playCatchingGame(){
  // update all the physics stuff (particles, springs, gravity)
  for (Bug b : catchingGame.mBugs) {
    clothesline.applyForce(physics, b);
  }

  for (Obstacle o : catchingGame.mObstacles) {
    clothesline.applyForce(physics, o);
  }
  
  physics.update();
  clothesline.display(physics);

  if (catchingGame.timeForNewItem(frameCount, assets.mGameLevel)) {
    //Create a new ellipse to add to clothesline. Do not place it too close to the clothesline supports.
    if (!physics.particles.isEmpty()) {
      //We want to avoid a certain set of particles. The first and last 4 so that the user can hit them.
      final int particlesToAvoid = 10;
      int particleId = (int)random(particlesToAvoid, physics.particles.size()-particlesToAvoid);
      Vec2D particlePos = physics.particles.get(particleId).getPreviousPosition().copy();
      //To add the weight we need to collect all weights.
      ArrayList<Weight> weights = new ArrayList<Weight>();
      for (Bug b : catchingGame.mBugs) {
        weights.add(b);
      }

      for (Obstacle o : catchingGame.mObstacles) {
        weights.add(o);
      }
      
      //Attach the next bug to this particle position.
      float itemWidth = assets.getItemWidth(nextItemIndex);
      float itemHeight = assets.getItemHeight(nextItemIndex);
      Ellipse tmpEllipse = new Ellipse();
      
      if (clothesline.addWeight(physics, itemWidth, itemHeight, particlePos, tmpEllipse, weights)) {
        //The bug is attached to this particle. 
        assets.addItem(catchingGame, nextItemIndex, tmpEllipse);
        getNextItem();
      }
    }
  }

  //Draw player control and all assets.
  if (!catchingGame.display(assets, frameCount, constants, physics)) {
    constants.GAME_OVER =true;
  }
}

//Give the player instructions on how to play the game.
void playHelp(){
  image(assets.mBlankIntroScreen, 0,0);
  fill(165,82,8);
  textAlign(CORNERS);
  textFont(constants.GAME_ROPE_FONT);
  
  String Catch1 = new String("Catch Game"); 
  String Catch2 = new String("Move the swatter left and right by");
  String Catch3 = new String("pressing anywhere any side of the swatter");
  String Catch4 = new String("Squash the bugs for points but");
  String Catch5 = new String("avoid the bombs and webs");
  text(Catch1, Catch1.length(), height/2 - 5.0*constants.FONT_SIZE);
  text(Catch2, Catch1.length(), height/2 - 4.0*constants.FONT_SIZE);
  text(Catch3, Catch1.length(), height/2 - 3.0*constants.FONT_SIZE);
  text(Catch4, Catch1.length(), height/2 - 2.0*constants.FONT_SIZE);
  text(Catch5, Catch1.length(), height/2 - 1.0*constants.FONT_SIZE);
  
  String Hang1 = new String("Hang Game"); 
  String Hang2 = new String("Hang as many bugs as possible");
  String Hang3 = new String("by pressing anywhere on the rope");
  String Hang4 = new String("avoid squashing bugs on the ground");
  String Hang5 = new String("In both games you have 3 chances");
  text(Hang1, Catch1.length(), height/2 + 1.0*constants.FONT_SIZE);
  text(Hang2, Catch1.length(), height/2 + 2.0*constants.FONT_SIZE);
  text(Hang3, Catch1.length(), height/2 + 3.0*constants.FONT_SIZE);
  text(Hang4, Catch1.length(), height/2 + 4.0*constants.FONT_SIZE);
  text(Hang5, Catch1.length(), height/2 + 5.0*constants.FONT_SIZE);
  
  textAlign(CENTER);
  textFont(constants.ROPE_FONT);
  assets.mBackButton.display();
}

//Create a black screen and write thanks to where everything came from. Don't worry about pictures.
void playCredits(){  
  //Util.playSound(assets.mIntroSong);
  image(assets.mBlankIntroScreen, 0,0);
  //image(assets.mBlankIntroScreen, 0.0,0.0);
  fill(165,82,8);
  textAlign(CORNERS);
  textFont(constants.GAME_ROPE_FONT);
  
  String ProcessingThanks1 = new String("First of all a big thanks to"); 
  String ProcessingThanks2 = new String("the Processing team for developing the");
  String ProcessingThanks3 = new String("Processing tools to make this possible");
  text(ProcessingThanks1, ProcessingThanks1.length(), height/2 - 5.0*constants.FONT_SIZE);
  text(ProcessingThanks2, ProcessingThanks1.length(), height/2 - 4.0*constants.FONT_SIZE);
  text(ProcessingThanks3, ProcessingThanks1.length(), height/2 - 3.0*constants.FONT_SIZE);
  String ToxiclibsThanks1 = new String("A big thanks to Karsten Schulz"); 
  String ToxiclibsThanks2 = new String("for the toxiclibs library");
  text(ToxiclibsThanks1, ProcessingThanks1.length(), height/2 - 2.0*constants.FONT_SIZE);
  text(ToxiclibsThanks2, ProcessingThanks1.length(), height/2 - constants.FONT_SIZE);
  
  String OpenClipArtThanks = new String("A big thanks to Openclipart for lots of art"); 
  text(OpenClipArtThanks, ProcessingThanks1.length(), height/2 +constants.FONT_SIZE);
  String OpenGameArtThanks1 = new String("A big thanks to Opengameart");
  String OpenGameArtThanks2 = new String("for a lot of art and sounds"); 
  text(OpenGameArtThanks1, ProcessingThanks1.length(), height/2 +2*constants.FONT_SIZE);
  text(OpenGameArtThanks2, ProcessingThanks1.length(), height/2 +3*constants.FONT_SIZE);
  
  textAlign(CENTER);
  textFont(constants.ROPE_FONT);
  assets.mBackButton.display();
}

//Just do today's high scores and don't worry about saving to the phone for all time highs.
void playHighScore(){
  //Util.playSound(assets.mIntroSong);
  image(assets.mBlankIntroScreen, 0,0);
  fill(165,82,8);
  //Big text
  textAlign(CORNERS);
  String highScoresText = new String("Highest scores for today");
  text(highScoresText, highScoresText.length(), height/2.0 - 4*constants.FONT_SIZE);
  //Make text slightly smaller.
  float extraSpace = 1.5;
  String[] titles = {"1st  ", "2nd  ", "3rd  ", "4th  ", "5th  "}; 
  for(int i=0; i < constants.HIGHEST_SCORES.length; ++i){
    //
    text(titles[i] + constants.HIGHEST_SCORES[i], highScoresText.length(), height/2.0 + (i+1)*constants.FONT_SIZE*extraSpace - 4*constants.FONT_SIZE);
    
  }
  textAlign(CENTER);
  assets.mBackButton.display();
}

//Precompute the next item to be added. As the levels go up we increase the difficulty. I.e. more heavy items or obstacles appear. 
//The bugs are ordered from lightest to heavist. We want around 10-12 levels before it gets too difficult.
void getNextItem() {
 
  //Normal case everything is equal.
  if((gameType == GameType.CATCHING_GAME && catchingGame.mObstacles.size() > constants.MAX_NUMBER_OF_OBSTACLES) || assets.mGameLevel < 2){
    int min = 0;
    int max = constants.NUMBER_OF_BUG_TYPES;
    if (gameType == GameType.CATCHING_GAME && catchingGame.mObstacles.size() <= constants.MAX_NUMBER_OF_OBSTACLES) {
      max += constants.NUMBER_OF_OBSTACLE_TYPES;
    }
    //As the levels get more difficult we tend to get more and more heavier bugs/or obstacles.
    //Adjust the probabilities of what one gets based on the level we are on.
    nextItemIndex = (int)random(min, max);
    return;
  }

  //The following is for when assets.mGameLevel < 4. We aim roughly so that you get 25% more heavier 
  //bugs if hanging game or 25% more bombs and spider webs for catching game.
  int min = 0;
  //Just make the heavier more likely.
  int easyFactor = 1;
  int hardFactor = 2;
  int numberOfHard = 1;
  int max = (constants.NUMBER_OF_BUG_TYPES-numberOfHard)*easyFactor + numberOfHard*hardFactor;
  if (gameType == GameType.CATCHING_GAME) {
    max =  constants.NUMBER_OF_BUG_TYPES*easyFactor + hardFactor*constants.NUMBER_OF_OBSTACLE_TYPES;
    numberOfHard = 2;
  }
  
  //Make it that you get 50% more heavier bugs if hanging game or 50% more bombs and spider webs for catching game.
  if(assets.mGameLevel < 6){
    //To make 75% more likely we multiply the non-desirable items number of integers by 4 and desirable by 3.
    easyFactor = 1;
    hardFactor = 3;
    max = (constants.NUMBER_OF_BUG_TYPES-numberOfHard)*easyFactor + numberOfHard*hardFactor;
    if (gameType == GameType.CATCHING_GAME) 
      max =  constants.NUMBER_OF_BUG_TYPES*easyFactor + hardFactor*constants.NUMBER_OF_OBSTACLE_TYPES;
  }
  
  //Make it that you get 75% more heavier bugs if hanging game or 75% more bombs and spider webs for catching game.
  if(assets.mGameLevel < 8){
    //To make 75% more likely we multiply the non-desirable items number of integers by 3 and desirable by 2. 
    easyFactor = 1;
    hardFactor = 4;
    max = (constants.NUMBER_OF_BUG_TYPES-numberOfHard)*easyFactor + numberOfHard*hardFactor;
    if (gameType == GameType.CATCHING_GAME) 
      max =  constants.NUMBER_OF_BUG_TYPES*easyFactor + hardFactor*constants.NUMBER_OF_OBSTACLE_TYPES;
    
  }
  
  //Make it that you get 2x more heavier bugs if hanging game or 2x more bombs and spider webs for catching game.
  if(assets.mGameLevel < 10){
    //To make 2x more likely we multiply the non-desirable items number of integers by 2 and leave the others alone
    easyFactor = 1;
    hardFactor = 5;
    max = (constants.NUMBER_OF_BUG_TYPES-numberOfHard)*easyFactor + numberOfHard*hardFactor;
    if (gameType == GameType.CATCHING_GAME) 
      max =  constants.NUMBER_OF_BUG_TYPES*easyFactor + hardFactor*constants.NUMBER_OF_OBSTACLE_TYPES;
    
  }
  //Final case. Make it that you get 3x more heavier bugs if hanging game or 3x more bombs and spider webs for catching game.
  //To make 2x more likely we multiply the non-desirable items number of integers by 3 and leave the others alone
  if(assets.mGameLevel >= 10){
    easyFactor = 1;
    hardFactor = 6;
    max = (constants.NUMBER_OF_BUG_TYPES-numberOfHard)*easyFactor + numberOfHard*hardFactor;
    if (gameType == GameType.CATCHING_GAME) 
      max =  constants.NUMBER_OF_BUG_TYPES*easyFactor + hardFactor*constants.NUMBER_OF_OBSTACLE_TYPES;
  }
  
  int randInt = (int)random(min, max);
  int numberEasyIterations = max - numberOfHard*hardFactor; 
  for(int i = 0; i < numberEasyIterations; ++i){
    if(randInt < (i+1)*easyFactor){
      nextItemIndex = i;
      return;
    }
  }
  
  int startIndex = numberEasyIterations/easyFactor;
  int cutOff = numberEasyIterations*easyFactor;
  for(int i = 0; i < numberOfHard; ++i){
    if(randInt < (i+1)*hardFactor + cutOff){
      nextItemIndex = startIndex + i;
      return;
    }
  }
}

//Display the next bug or obstacle, text, level and score
void displayAllInfo() {
  textFont(constants.GAME_ROPE_FONT);
  PImage nextItem = assets.getNextImage(nextItemIndex);
  image(nextItem, constants.NEXT_BUG_XPOSITION, height/240.0);
  fill(165,82,8);
  text(constants.mNextBugText, constants.NEXT_XPOSITION, constants.FONT_SIZE*0.6 + constants.TOP_DISPL);
  text(constants.mLevelText + assets.mGameLevel, constants.LEVEL_XPOSITION, constants.FONT_SIZE*0.6 + constants.TOP_DISPL);
  text(constants.mScoreText + constants.GAME_SCORE, constants.SCORE_XPOSITION, constants.FONT_SIZE*0.6 + constants.TOP_DISPL);
  textFont(constants.ROPE_FONT);
}

//Initialize the clothesline so that it is close to equilibrium when the player starts.
void initializePhysics() {
  for (int i =0; i < 200; ++i) {
     physics.update();
  }
}

//To free up memory we clear the background image and reload the game images
void loadGameImages(){
  assets.clearIntroImages();
  imgLoader.loadGameImages(constants.NUMBER_OF_BUG_TYPES, assets, constants.getBannerHeight(), gameType);
  if(gameType == GameType.CATCHING_GAME){
    assets.addLifeBugs(catchingGame);
  }
  if(gameType == GameType.HANGING_GAME){
    assets.addLifeBugs(hangingGame);
  }
  constants.setBannerText(assets.mLifeBug.getLook().width * 3);
}

//To free up memory we clear the game images and reload the background image
void clearGameImages(){
  assets.clearGameImages();
  if(gameType == GameType.CATCHING_GAME){
    catchingGame.mCatcher.mImages.clear();
  }
  imgLoader.loadIntroImages(assets);
}

//Allow user to drag their finger on the screen.
void mouseDragged(){
  if(firstTime) 
    return;
  
  Vec2D mouseLoc = new Vec2D(mouseX, mouseY);
  if (gameType == GameType.CATCHING_GAME){
    if (!catchingGame.mCatcher.isStuck()) {
      if(mouseLoc.x < catchingGame.mCatcher.mPosition.x){
        catchingGame.mCatcher.moveLeft(frameCount);
      }else{
        catchingGame.mCatcher.moveRight(frameCount);
      }
    }
  }
}

//Try to add a new weight to the clothesline. 
void mousePressed() {
  if (constants.GAME_OVER == true) {
    restartGame();
    return;
  }
  
  Vec2D mouseLoc = new Vec2D(mouseX, mouseY);
  if (assets.mBackButton.buttonPressed(mouseLoc)) {
    constants.updateHighScores(constants.GAME_SCORE);
    if(gameType == GameType.CATCHING_GAME || gameType == GameType.HANGING_GAME){
      clearGameImages();
    }
    gameType = GameType.INTRO;
    restartGame();
    return;
  }
  
  if(gameType == GameType.INTRO){
    introButtons.pressed(mouseLoc);
  }
  
  //For catching game move the catcher left and right.
  if (!firstTime && gameType == GameType.CATCHING_GAME){
    if (!catchingGame.mCatcher.isStuck()) {
      if(mouseLoc.x < catchingGame.mCatcher.mPosition.x){
        catchingGame.mCatcher.moveLeft();
      }else{
        catchingGame.mCatcher.moveRight();
      }
    }
  }

  //For the hanging game, hang a new bug on the rope.
  if (!firstTime && gameType == GameType.HANGING_GAME) {
    float bugWidth = assets.getItemWidth(nextItemIndex);
    float bugHeight = assets.getItemHeight(nextItemIndex);
    Ellipse tmpEllipse = new Ellipse();
    
    ArrayList<Weight> weights = new ArrayList<Weight>();
    for (Bug b : hangingGame.mBugs) {
      weights.add(b);
    }
    
    if (hangingGame.mouseAction(clothesline, physics, bugWidth, bugHeight, mouseLoc, tmpEllipse, weights)) {
      //The bug is attached to this particle. 
      assets.addItem(hangingGame, nextItemIndex, tmpEllipse);
      //Avg Size determines weight so it should be added to the score.
      float score = assets.getAvgSize(nextItemIndex);
      constants.GAME_SCORE += score;
      getNextItem();
    } else {
      return;
    }

    if (!hangingGame.enoughSpaceLeft(clothesline, bugWidth)) {
      //Just give the player a bonus if they fill the clothesline
      hangingGame.clearAll(false);
      //Get bonus points for filling the clothesline.
      float fillBonus = 100.0;
      constants.GAME_SCORE += fillBonus;
      hangingGame.displayBonus();
      
    }
  }
}

//Load sounds using Android code
void loadSounds() {
  
  AssetFileDescriptor explosionFD, appearFD, levelUpFD, squashFD;
  assets.mBugAppear = new MediaPlayer();
  try{
    appearFD = context.getAssets().openFd("bug_appear.mp3");//which is in the data folder
    assets.mBugAppear.setDataSource(appearFD.getFileDescriptor(), appearFD.getStartOffset(), appearFD.getLength());
    assets.mBugAppear.prepare();
  }catch(Exception e){
  }
  
  assets.mLevelUp = new MediaPlayer();
  try{
    levelUpFD = context.getAssets().openFd("levelUp.mp3");//which is in the data folder
    assets.mLevelUp.setDataSource(levelUpFD.getFileDescriptor(), levelUpFD.getStartOffset(), levelUpFD.getLength());
    assets.mLevelUp.prepare();
  }catch(Exception e){
  }
    
  assets.mExplosion = new MediaPlayer();
  try{
    explosionFD = context.getAssets().openFd("explosion.mp3");//which is in the data folder
    assets.mExplosion.setDataSource(explosionFD.getFileDescriptor(), explosionFD.getStartOffset(), explosionFD.getLength());
    assets.mExplosion.prepare();
  }catch(Exception e){
  }
  
  assets.mSquashedBugSound = new MediaPlayer();
  try{
    squashFD = context.getAssets().openFd("squash.mp3");//which is in the data folder
    assets.mSquashedBugSound.setDataSource(squashFD.getFileDescriptor(), squashFD.getStartOffset(), squashFD.getLength());
    assets.mSquashedBugSound.prepare();
  }catch(IOException e){
  }
}

void restartGame(){
  if (gameType == GameType.CATCHING_GAME) {
    catchingGame.clearAll();
  }
  if (gameType == GameType.HANGING_GAME) {
    hangingGame.clearAll(true);
  }
  constants.reset();
  assets.gameReset();
  nextItemIndex = 0;
}

//Restart the game if game is over. Do not need this for tablets.
//void keyPressed() {
//  if (constants.GAME_OVER == true) {
//    restartGame();
//    return;
//  }

//  if (gameType == GameType.CATCHING_GAME) {
//    if (!catchingGame.mCatcher.isStuck()) {
//      //Move the catching ellipse if the user has pressed an arrow key.
//      if (key == CODED) {

//        if (keyCode == LEFT) {
//          catchingGame.mCatcher.moveLeft();
//        }  

//        if (keyCode == RIGHT) {
//          catchingGame.mCatcher.moveRight();
//        }
//      }
//    }
//  }
//}