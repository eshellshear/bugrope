//This class is to display a visible constraint for toxiclibs. It is used for the supports of the rope.

public class VisibleRectConstraint extends RectConstraint {

  //Constructor
  public VisibleRectConstraint(Vec2D min, Vec2D max) {
    super(min, max);
  }

  //Draw the rectangle.
  public void draw() {
    Vec2D m=rect.getBottomRight();
    Vec2D n=rect.getTopLeft();
    beginShape(QUAD_STRIP);
    stroke(0);
    vertex(m.x, m.y);
    vertex(n.x, m.y); 
    vertex(m.x, n.y);
    vertex(n.x, n.y);
    endShape();
  }

  public void display(PImage iImage) {
    Vec2D n=rect.getTopLeft();
    image(iImage, n.x, n.y);
  }
}