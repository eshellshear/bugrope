//A class to represent obstacles for the CatchingGame. Things like spider webs, bombs, etc.

abstract class Obstacle extends Weight {

  //Variables 
  boolean mCaught; //A variable to represent whether the player has been caught by the obstacle or not

  //Constructor
  Obstacle() {
    super();
    mCaught = false;
  }
  
  Obstacle(Vec2D iWeight, Ellipse iEllipse){
    super(iWeight, iEllipse);
  }

  //Methods
  //Did we get the catcher.
  boolean gotCatcher(Catcher iCatcher) {
    float distanceSqrd = iCatcher.mPosition.distanceToSquared(mEllipse.mPosition);
    if (distanceSqrd < sq(mEllipse.getEllipseSize() + iCatcher.getEllipseSize())) {
      mCaught = true;
      return true;
    }
    return false;
  }

  void setCaught(boolean iCaught) {
    mCaught = iCaught;
  }
  
  boolean isCaught(){
   return mCaught; 
  }  
  //The termination criteria to know when the obstacle can be removed from the game.
  abstract boolean isFinished();
  //Display what happens to the catcher depending on obstacle type. Overriden by the extending classes.
  abstract void display(float iFrameCount);
  //To load the images corresponding to this obstacle
  abstract void addImage(String iPath);
  //Get the prototype image for this object
  abstract PImage getImage();
  //Return whether bomb
  abstract boolean isBomb();
  //Return whether a web
  abstract boolean isWeb();
  //Return the width of the object
  abstract float getWidth();
  //Return the height of the object
  abstract float getHeight();
  //Return the avg size of the object
  abstract float getAvgSize();
  //Return the image of the object
  abstract PImage getLook();
  
  
}